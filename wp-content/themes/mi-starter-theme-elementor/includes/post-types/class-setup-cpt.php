<?php
/**
 * The template for Create Custom Post Type used in WP-admin
 * 
 * Author: Muhammad Rizki
 * 
 * Note : tinggal bikin fungsi baru lalu panggil di fungsi setupCPT
 *
 * @package HelloElementor
 */
defined( 'ABSPATH' ) || die( "Can't access directly" );

class SetupCPT
{
    /**
     * setup your custom post type in here
     */
    protected function setupCPT()
    {
        $setup_data = [
          'contoh-1' => $this->cptExample1(),
          'contoh-2' => $this->cptExample2(),
          'contoh-3' => $this->cptAndi(),

        ];
        return $setup_data;
    }

    private function cptExample1()
    {
        $cpt =   [
            'plural'            => 'ExamplesCPT 1',
            'singular'          => 'ExampleCPT 1',
            'slug'              => 'example-cpt-1',
            'domnain'           => THEME_DOMAIN,
            'supports'          => ['title', 'editor', 'thumbnail'],
            'menu_positions'    => 6,
            'has_archive'       => true,
            'capability_type'   => 'post',
            'has_taxonomy'      => true,
            'taxonomy'          => [
                [
                    'slug_taxonomy'       => 'taxonomy-cpt-1',
                    'taxonomy_register' => [
                        'label' => __('Taxonomy 1'),
                        'rewrite' => array('slug' => 'taxonomy-cpt-1'),
                        'hierarchical' => true,
                        'singular_name' => __('Taxonomy 1'),
                        'query_var' => true 
                    ],
                ],

                [
                    'slug_taxonomy'       => 'taxonomy-cpt-2',
                    'taxonomy_register' => [
                        'label' => __('Taxonomy 2'),
                        'rewrite' => array('slug' => 'taxonomy-cpt-2'),
                        'hierarchical' => true,
                        'singular_name' => __('Taxonomy 2'),
                        'query_var' => true 
                    ],
                ],
                
            ]
        ];
        return $cpt;
    }

    private function cptExample2()
    {
        $cpt = [
            'plural'            => 'ExamplesCPT 2',
            'singular'          => 'ExampleCPT 2',
            'slug'              => 'example-cpt-2',
            'domnain'           => THEME_DOMAIN,
            'supports'          => ['title', 'editor', 'author','thumbnail', 'excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'],
            'menu_positions'    => 7,
            'has_archive'       => true,
            'capability_type'   => 'post',
            'taxonomy'          => false
        ];
        return $cpt;
    }

    private function cptAndi()
    {
	    $cpt = [
		    'plural'            => 'CPT ANDI',
		    'singular'          => 'CPT ANDI',
		    'slug'              => 'cpt-andi',
		    'domnain'           => THEME_DOMAIN,
		    'supports'          => ['title', 'editor', 'author','thumbnail', 'excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'],
		    'menu_positions'    => 7,
		    'has_archive'       => true,
		    'capability_type'   => 'post',
		    'has_taxonomy'      => true,
		    'taxonomy'          => [
			    [
				    'slug_taxonomy'       => 'taxonomy-cpt-andi',
				    'taxonomy_register' => [
					    'label' => __('Taxonomy Andi'),
					    'rewrite' => array('slug' => 'taxonomy-cpt-andi'),
					    'hierarchical' => true,
					    'singular_name' => __('Taxonomy Andi'),
					    'query_var' => true
				    ],
			    ],


		    ]
	    ];
	    return $cpt;
    }
}
