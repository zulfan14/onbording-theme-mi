<?php
/**
 * The template for Crete Custom Post Type used in WP-admin
 * 
 * Author: Muhammad Rizki
 * 
 * Note : jangan sentuh class ini
 *
 * @package HelloElementor
 */
defined( 'ABSPATH' ) || die( "Can't access directly" );

require_once __DIR__ . '/class-setup-cpt.php';


class RegisterCPT extends SetupCPT
{
    public function __construct()
    {
        $this->hook();
    }

    public function hook()
    {
        add_action('init',[$this, 'CustomPostTypeRegister']);
    }

    /**
     * Custom Post Type
     */
    public function CustomPostTypeRegister()
    {
        $setupCPT = $this->setupCPT();

        foreach ($setupCPT as $setup) {

            $labels = $this->_lable($setup);
            $args = $this->_args($setup, $labels);
            register_post_type($setup['slug'], $args);
            $this->_taxonomy($setup);
        }
    }

    /**
     * setup Lables
     */
    private function _lable(array $setup)
    {
        $lable = [
            'name'                  => _x($setup['plural'], $setup['plural'].' General Name', $setup['domain']),
            'singular_name'         => _x($setup['singular'], 'Post Type Singular Name', $setup['domain']),
            'menu_name'             => __($setup['plural'], $setup['domain']),
            'name_admin_bar'        => __($setup['singular'], $setup['domain']),
            'archives'              => __($setup['singular'].' Archives', $setup['domain']),
            'attributes'            => __($setup['singular'].' Attributes', $setup['domain']),
            'parent_item_colon'     => __('Parent '.$setup['singular'].':', $setup['domain']),
            'all_items'             => __('All '.$setup['plural'], $setup['domain']),
            'add_new_item'          => __('Add New '.$setup['singular'], $setup['domain']),
            'add_new'               => __('Add New', $setup['domain']),
            'new_item'              => __('New '.$setup['singular'], $setup['domain']),
            'edit_item'             => __('Edit '.$setup['singular'], $setup['domain']),
            'update_item'           => __('Update '.$setup['singular'], $setup['domain']),
            'view_item'             => __('View '.$setup['singular'], $setup['domain']),
            'view_items'            => __('View '.$setup['plural'], $setup['domain']),
            'search_items'          => __('Search '.$setup['plural'], $setup['domain']),
            'not_found'             => __('Not found', $setup['domain']),
            'not_found_in_trash'    => __('Not found in Trash', $setup['domain']),
            'featured_image'        => __('Featured Image', $setup['domain']),
            'set_featured_image'    => __('Set featured image', $setup['domain']),
            'remove_featured_image' => __('Remove featured image', $setup['domain']),
            'use_featured_image'    => __('Use as featured image', $setup['domain']),
            'insert_into_item'      => __('Insert into item', $setup['domain']),
            'uploaded_to_this_item' => __('Uploaded to this '.$setup['singular'], $setup['domain']),
            'items_list'            => __($setup['singular'].' list', $setup['domain']),
            'items_list_navigation' => __($setup['singular'].' list navigation', $setup['domain']),
            'filter_items_list'     => __('Filter '.$setup['singular'].' list', $setup['domain']),
        ];

        return $lable;
    }

    /**
     * setup Args
     */
    private function _args(array $setup,array $labels)
    {
        $args = [
            'label'                 => __($setup['singular'], $setup['domain']),
            'description'           => __($setup['singular'].' Description', $setup['domain']),
            'labels'                => $labels,
            'supports'              => $setup['supports'],
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => $setup['menu_positions'],
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => $setup['has_archive'],
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => $setup['capability_type'],
        ];
        
        return $args;
    }

    /**
     * setup Taxonomy
     */
    private function _taxonomy(array $setup)
    {
        if( $setup['has_taxonomy']) {
            foreach ($setup['taxonomy'] as $taxonomy) {;
                register_taxonomy(
                    $taxonomy['slug_taxonomy'],
                    $setup['slug'],
                    $taxonomy['taxonomy_register']
                );
            }
        }         
    }
}


// initialize
new RegisterCPT();