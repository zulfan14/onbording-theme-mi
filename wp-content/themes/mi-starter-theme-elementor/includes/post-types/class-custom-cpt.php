<?php
/**
 * The template for Create Custom Post Type used in WP-admin
 * 
 * Author: Muhammad Rizki
 * 
 * Note : kalau cpt mau di custom di file ini
 *
 * @package HelloElementor
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );

require_once __DIR__ . '/class-setup-cpt.php';

class CustomCPT extends SetupCPT
{
    public function __construct()
    {
        $this->hook();
        flush_rewrite_rules( false );
    }

    /**
     * masukan semua hook kedalam sini
     */
    public function hook()
    {
        $setup = $this->setupCPT();

        add_filter('manage_'.$setup['contoh-3']['slug'].'_posts_columns', [$this, 'costum_colums_example_cpt_1']);
    }

    public function costum_colums_example_cpt_1($columns)
    {
        $columns['Kolom_A'] = __('Kolom A',THEME_DOMAIN);
        $columns['Kolom_B'] = __('Kolom B',THEME_DOMAIN);
        return $columns;
    }
}

/**
 * initialize
 */
new CustomCPT();