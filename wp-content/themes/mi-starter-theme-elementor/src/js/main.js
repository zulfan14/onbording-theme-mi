import $ from "jquery";
window.$ = window.jQuery = $;
import handler from "./customs/ExampleHandler.js";
import constant from "./constant.js";
import SlickHandler from "./customs/SlickHandler.js";

(function ($) {
	// console.log(constant.API_KEY);
	// console.log(handler.arrayHandler());
	// // handler.ajaxRequest();
	// SlickHandler.createSlick();
	// handler.changeText();
	handler.main();
})(jQuery);
