<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'onboarding_db' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'kSj6=QCExz;e3Xu`?Lk3=B/Mu=}gX7kEz5z}[vK#,!^!vl p>jaq}/2i/W_k/$.Z' );
define( 'SECURE_AUTH_KEY',  'hWc;I- {L)L~HeNxkyXLnw))IB@6s|:OO=pozVd3[5at8Q|Un`:^N;24HXAwuNOE' );
define( 'LOGGED_IN_KEY',    '?&zEB,}m<p(9 VRuG-EuoZ/^[bBX}>6R|3P<f8_nXIXsc6)/^5EpYx?-I3(xmk /' );
define( 'NONCE_KEY',        'uV6N^7/ CzE:{4=@q3f2cb}1D6#TnY(PiBKNon1W:3D)@g<5!8wlA-FeqO$=vwN}' );
define( 'AUTH_SALT',        'E{q{n8A2&:>qx?eo.^}%l*B!nO|p4ygKQvotyCyewL(F8k8E%#l,g9{K/,2L,7/2' );
define( 'SECURE_AUTH_SALT', 'A~*SL0]eiWWKWtpMuxGB{Tj=tdlvF;r`7x=MY-xu]_tI-NAuL,u-a{u:8e>Dj&9w' );
define( 'LOGGED_IN_SALT',   '&]<2jg%,(x7Bm{>Jg9(Dy;IU-=O:]nl@h_]msWJz=%k!:=me`}kTOo[_MD67%dK=' );
define( 'NONCE_SALT',       'N}]*Wpo~7uhO `?.pUa@{dnq@).9M:jbUXg_M{8(mj:b+1/6LVu[N@C=`e2ir=ck' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
